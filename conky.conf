conky.config = {
--==============================================================================
--  2021/10/17
--  this theme is for conky version 1.10.8 or newer
-- 
--  Simplest
--
--  author  : trandat.hust@gmail.com
--  license : Distributed under the terms of MIT License

--==============================================================================
	update_interval = 1,
	cpu_avg_samples = 2,
	net_avg_samples = 2,
	out_to_console = false,
	override_utf8_locale = true,
	double_buffer = true,
	no_buffers = true,
	text_buffer_size = 32768,
	imlib_cache_size = 0,
	own_window = true,
	own_window_type = 'normal',
	own_window_argb_visual = true,
	own_window_argb_value = 50,
	own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
	border_inner_margin = 5,
	border_outer_margin = 0,
	xinerama_head = 1,
	alignment = 'bottom_right',
	gap_x = 0,
	gap_y = 33,
	draw_shades = false,
	draw_outline = false,
	draw_borders = false,
	draw_graph_borders = false,
	use_xft = true,
	font = 'Ubuntu Mono:size=12',
	xftalpha = 0.8,
	uppercase = false,
	default_color = 'white',
	own_window_colour = '#000000',
	minimum_width = 300, minimum_height = 0,
	alignment = 'top_right',

};
conky.text = [[
${execi 300 ~/.conky/scripts/weather.sh}\
${execi 300 ~/.conky/scripts/cryptocurrency.sh}\

${time %H:%M:%S}${alignr}${time %d-%m-%y}
${voffset -16}${font sans-serif:bold:size=18}${alignc}${time %H:%M}${font}
${voffset 4}${font sans-serif:bold:size=10}${alignc}${time %A, %d %B %Y}

${font}${voffset -4}
${font sans-serif:bold:size=10}WEATHER ${hr 2}
${font sans-serif:bold:size=10}${execi 100 cat ~/.cache/weather.json | jq -r '.name'}
${font sans-serif:bold:size=6}${execi 100 cat ~/.cache/weather.json | jq '.main.temp' | awk '{print int($1+0.5)}'}° ${execi 100 cat ~/.cache/weather.json | jq -r '.weather[0].description' | sed -e 's/\(.*\)/\U\1/'}${font sans-serif:size=1}
${execi 300 ~/.config/conky/Graffias/scripts/weather.sh}\
${font sans-serif:size=8}Max temperature: $alignr${font sans-serif:size=8}${execi 100 cat ~/.cache/weather.json | jq '.main.temp_max' | awk '{print int($1+0.5)}'}°C
${font sans-serif:size=8}Wind speed: $alignr${execi 100 (cat ~/.cache/weather.json | jq '.wind.speed')}km/h
${font sans-serif:size=8}Humidity: $alignr${execi 100 (cat ~/.cache/weather.json | jq '.main.humidity')}% 

${font sans-serif:bold:size=10}CRYPTO ${hr 2}
${font sans-serif:size=8}BTC: $alignr ${offset -30}${execi 100 cat ~/.cache/cryptocurrency/BTCUSDT.json | jq -r '.data.o'}USDT $alignr ${font sans-serif:size=8}ETH: $alignr ${execi 100 cat ~/.cache/cryptocurrency/ETHUSDT.json | jq -r '.data.o'}USDT 
${font sans-serif:size=8}BNB: $alignr ${offset -45}${execi 100 cat ~/.cache/cryptocurrency/BNBUSDT.json | jq -r '.data.o'}USDT $alignr ${offset -15}${font sans-serif:size=8}FTX: $alignr ${execi 100 cat ~/.cache/cryptocurrency/FTTUSDT.json | jq -r '.data.o'}USDT
${offset 0}${font sans-serif:size=8}ADA: $alignr ${offset -55}${execi 100 cat ~/.cache/cryptocurrency/ADAUSDT.json | jq -r '.data.o'}USDT $alignr ${offset -18}${font sans-serif:size=8}LINK: $alignr ${execi 100 cat ~/.cache/cryptocurrency/LINKUSDT.json | jq -r '.data.o'}USDT

${font sans-serif:bold:size=10}SYSTEM ${hr 2}
${font sans-serif:normal:size=8}$sysname $kernel $alignr $machine
Host:$alignr$nodename
Uptime:$alignr$uptime
File System: $alignr${fs_type}
Processes: $alignr ${execi 1000 ps aux | wc -l}

${font sans-serif:bold:size=10}CPU ${hr 2}
${font sans-serif:normal:size=8}${execi 1000 grep model /proc/cpuinfo | cut -d : -f2 | tail -1 | sed 's/\s//'}

${font sans-serif:normal:size=8}CPU Average  $alignc ${cpu} %
$cpubar

1: ${cpu cpu1}% ${cpubar cpu1 8,100} $alignr 7: ${cpu cpu7}% ${cpubar cpu7 8,100}
2: ${cpu cpu2}% ${cpubar cpu2 8,100} $alignr 8: ${cpu cpu8}% ${cpubar cpu8 8,100}
3: ${cpu cpu3}% ${cpubar cpu3 8,100} $alignr 9: ${cpu cpu9}% ${cpubar cpu9 8,100}
4: ${cpu cpu4}% ${cpubar cpu4 8,100} $alignr 10: ${cpu cpu10}% ${cpubar cpu10 8,100}
5: ${cpu cpu5}% ${cpubar cpu5 8,100} $alignr 11: ${cpu cpu11}% ${cpubar cpu11 8,100}
6: ${cpu cpu6}% ${cpubar cpu6 8,100} $alignr 12: ${cpu cpu12}% ${cpubar cpu12 8,100}


${font sans-serif:bold:size=10}MEMORY ${hr 2}
${font sans-serif:normal:size=8}RAM $alignc $mem / $memmax $alignr $memperc%
$membar
SWAP $alignc ${swap} / ${swapmax} $alignr ${swapperc}%
${swapbar}

${font sans-serif:bold:size=10}DISK USAGE ${hr 2}
${font sans-serif:normal:size=8}/ $alignc ${fs_used /} / ${fs_size /} $alignr ${fs_used_perc /}%
${fs_bar /}

${font Ubuntu:bold:size=10}NETWORK ${hr 2}
${font sans-serif:normal:size=8}Local IPs:${alignr}External IP:
${execi 1000 ip a | grep inet | grep -vw lo | grep -v inet6 | cut -d \/ -f1 | sed 's/[^0-9\.]*//g'}  ${alignr}${execi 1000  wget -q -O- http://ipecho.net/plain; echo}
${font sans-serif:normal:size=8}Down: ${downspeed enp0s3}  ${alignr}Up: ${upspeed enp0s3} 

${font sans-serif:bold:size=10}TOP PROCESSES ${hr 2}
${font sans-serif:normal:size=8}Name $alignr PID   CPU%   MEM%${font sans-serif:normal:size=8}
${top name 1} $alignr ${top pid 1} ${top cpu 1}% ${top mem 1}%
${top name 2} $alignr ${top pid 2} ${top cpu 2}% ${top mem 2}%
${top name 3} $alignr ${top pid 3} ${top cpu 3}% ${top mem 3}%
${top name 4} $alignr ${top pid 4} ${top cpu 4}% ${top mem 4}%
${top name 5} $alignr ${top pid 5} ${top cpu 5}% ${top mem 5}%
${top name 6} $alignr ${top pid 6} ${top cpu 6}% ${top mem 6}%
${top name 7} $alignr ${top pid 7} ${top cpu 7}% ${top mem 7}%
${top name 8} $alignr ${top pid 8} ${top cpu 8}% ${top mem 8}%
${top name 9} $alignr ${top pid 9} ${top cpu 9}% ${top mem 9}%
${top name 10} $alignr ${top pid 10} ${top cpu 10}% ${top mem 10}%
]];