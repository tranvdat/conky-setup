#!/bin/bash

# get currency by binance
symbols=(BTCUSDT ETHUSDT BNBUSDT FTTUSDT ADAUSDT LINKUSDT)

if [ ! -d "~/.cache/cryptocurrency" ]; then
    mkdir ~/.cache/cryptocurrency
fi

for i in "${symbols[@]}"
do
    url="https://www.binance.com/bapi/asset/v2/public/asset-service/product/get-product-by-symbol?symbol=${i}"
    curl ${url} -s -o ~/.cache/cryptocurrency/${i}.json
done


