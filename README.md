## Base source
- https://github.com/brndnmtthws/conky

## Screenshots

![preview](./preview.png)


## Quickstart

```ShellSession
$ sudo apt install conky-all
```
Enable Conky to start at boot

```ShellSession
$ cp ./conky.conf ~/.conkyrc
$ cp -r scripts ~/.conky
$ reboot
```


## Documentation
- https://github.com/brndnmtthws/conky

## License

Conky Setup is licensed under the terms of the [GPLv3](LICENSE) license.

